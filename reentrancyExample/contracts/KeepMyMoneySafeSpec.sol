pragma solidity ^0.4.4;
import './Specification.sol';


//this specification will work on ReentrancyVulnerable. The goal is to specify what is of interest for the user, rather 
// than refer to the anticipated bug in any way. currently it will communicate to its own version of contract 
//  (so the contract and the spec are tightly bound)

// this whole thing seem weird: it is basically mimicking the functionality of the original contract (basically, only abstracting
//the important events from it. What's the point? 1. readability 2. claim that on the external service nothing can go wrong and
// we're going to send the info just before that happens. 
contract KeepMyMoneySafeSpec is Specification
{
	
	event MoneyWithdrawnEvent();
	mapping (address => uint) private userBalances;
	bool okToPay;
	//uint totalMoney;
	//uint[] users;
	
	function KeepMyMoneySafe()
	{
		okToPay = true;
	}
	
	
	
	function moneyStored(address user, uint userBalance)
	{
		userBalances[user] = userBalance;
	}
	
	function moneyToWithdraw(address user, uint amount)
	{
		okToPay = true;
		// this check already should be enough as no money withdrawal could happen  without the check. however, 
		// we're assuming that we don't know much details about what could happen so we're adding more functionality.
		if(userBalances[user] < amount){
			okToPay = false;
		}
		userBalances[user] -= amount;
		
	}

	
	function moneyWithdrawn(address user, uint amount)
	{
		MoneyWithdrawnEvent();
	}
	
	function isItOK() returns (bool ok)
	{
		if (okToPay == false)
			return false;
		else
			return true;
	}
	
		
}
