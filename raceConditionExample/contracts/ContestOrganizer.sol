pragma solidity ^0.4.4; //although I don't have specific requirements, but compiler complains if the pragma is not specified

contract ContestOrganizer
{
	uint prizeForTheCorrectSolution;
	uint square = 36;
	uint prize = 200000000000000000;
	event ReceivedSomeMoney();
	event ReceivedCorrectSolution();
	event ReceivedSomeSolution(uint guess);
	event prizeChanged(uint newValue);
	
	function() payable
	{
		ReceivedSomeMoney();
		
	}
	
	function ContestOrganizer()
	{
		
	}
	
	function checkSolution(uint x) private returns (bool correct)
	{
		if (x*x == square){
			return true;
		}
		else{
			return false;
		}
	}
	
	function getPrizeAmount() returns (uint)
	{
		return prize;
	}
	
	function changePrizeAmount(uint newValue) 
	{
		prize = newValue;
		prizeChanged(newValue);
	}
	
	function participateInContest(uint solution)
	{
		ReceivedSomeSolution(solution);
		if (checkSolution(solution) == true){
			ReceivedCorrectSolution();
			//msg.sender.call.value(prize).gas(80000)();
			msg.sender.transfer(prize);
		}
	}
}
