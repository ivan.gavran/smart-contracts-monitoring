pragma solidity ^0.4.4; //although I don't have specific requirements, but compiler complains if the pragma is not specified
import './ContestOrganizer.sol';

contract ContestOrganizerSecretContract
{
	ContestOrganizer organizerContract;
	event MoneyDroppingIn();
	
	function () payable
	{
		MoneyDroppingIn();
	}
	
	function ContestOrganizerSecretContract(address organizersAddress)
	{
		organizerContract = ContestOrganizer(organizersAddress);
	}
	
	function pingFromTheOutside()
	{
		organizerContract.changePrizeAmount(10000000000000000);
	}
	
	
	
}
