var ContestOrganizerContract = artifacts.require("./ContestOrganizer.sol");
var SecretContract = artifacts.require("./ContestOrganizerSecretContract.sol");
var ContestParticipantContract = artifacts.require("./ContestParticipant.sol")
contract('RaceCondition', function(accounts) {
  
   // check whether the contract upon deployment has the right (0) value per some non-specific user
  var organizerInstance;
  var secretInstance;
  var participantInstance;
  var initialDonations = 5;
  var gasEnough = 80000;
  
  
  
  it("testing typical race condition fraud", function(){
	  
	  return ContestOrganizerContract.new()
	  .then(function(instance){
		  organizerInstance = instance;
		  return instance.sendTransaction({value: web3.toWei(initialDonations, "ether"), gas:gasEnough});
	  })
	  .then(function(){
		  assert.equal(web3.eth.getBalance(organizerInstance.address).valueOf(), web3.toWei(initialDonations,"ether"), "not the right amount of money");
		  return SecretContract.new(organizerInstance.address)
	  })
	  .then(function(instance){
		  secretInstance = instance;
		  return instance.sendTransaction({value: web3.toWei(initialDonations, "ether"), gas:gasEnough});
	  })
	  .then(function(){
		  assert.equal(web3.eth.getBalance(secretInstance.address).valueOf(), web3.toWei(initialDonations,"ether"), "not the right amount of money");
		  return ContestParticipantContract.new(organizerInstance.address);
	  })
	  .then(function(instance){
		  participantInstance = instance;
		  return instance.sendTransaction({value: web3.toWei(initialDonations, "ether"), gas:gasEnough});
	  })
	  .then(function(){
		  assert.equal(web3.eth.getBalance(participantInstance.address).valueOf(), web3.toWei(initialDonations,"ether"), "not the right amount of money");
		  return organizerInstance.getPrizeAmount.call();
	  })
	  .then(function(prizeValue){
		  if (prizeValue > 10000000000000000){
			  return participantInstance.hardWork();
		  }
	  })
	  .then(function(){
		  console.log("hard work has been done");
		  return secretInstance.pingFromTheOutside();
	  })
	  .then(function(){
		  console.log("about to participate");
		  return participantInstance.participate();
	  })
	  .then(function(){
		 console.log(web3.eth.getBalance(participantInstance.address).valueOf()); 
		 assert.equal(1,2,"dummy");
	  });
	  
  });
});


