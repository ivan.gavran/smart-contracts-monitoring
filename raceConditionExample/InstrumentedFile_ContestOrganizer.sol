import './InstrumentedFile_ContestOrganizer.sol';
pragma solidity ^0.4.4; //although I don't have specific requirements, but compiler complains if the pragma is not specified

contract ContestOrganizer
{
	Monitoring_ContestOrganizer public checker;
uint prizeForTheCorrectSolution;
	uint square = 36;
	uint prize = 200000000000000000;
	event ReceivedSomeMoney();
	event ReceivedCorrectSolution();
	event ReceivedSomeSolution(uint guess);
	event prizeChanged(uint newValue);
	function() payable
	{
		ReceivedSomeMoney();
		
	}
	
	function ContestOrganizer()
	{
checker = new Monitoring_ContestOrganizer();
	
		
	}
	
	function checkSolution(uint x) private returns (bool correct)
	{
		if (x*x == square){
			
checker.return_26_3_(true);
return true;
		}
		else{
			
checker.return_29_3_(false);
return false;
		}
	}
	
	function getPrizeAmount() returns (uint)
	{
		
checker.return_35_2_(prize);
return prize;
	}
	
	function changePrizeAmount(uint newValue) 
	{
		prize = newValue;
		prizeChanged(newValue);
	}
	
	function participateInContest(uint solution)
	{
		ReceivedSomeSolution(solution);
		if (
checker.checkSolution_47_6_call(solution);
checkSolution(solution) == true){
			ReceivedCorrectSolution();
			//msg.sender.call.value(prize).gas(80000)();
			msg.sender.transfer(prize);
		}
	}
	
	
}